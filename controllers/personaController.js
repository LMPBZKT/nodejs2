const { leerDB, guardarDB } = require('../helpers/guardarArchivo');
const Persona = require('../models/persona');
const Personas = require('../models/personas');
const { request, response } = 'express';

const personas = new Personas();

let personaDB = leerDB ();

if (personaDB) {
  personas.cargarPersonasFromArray(personaDB);
}

const personaGet = (req = request, res = response) => {


  let { nombres, apellidos } = req.query;

  if (nombres && apellidos) {
    let mostrarNomApe = personas.NombreApellido(nombres, apellidos)
    res.json({
      mostrarNomApe
    })
    
  } else {
    res.json({
      personaDB
    })
  }
}

const ciSexoPersonaGet = (req = request, res= response) => {  
  let parametro = req.params.parametro;

  if (parseInt(parametro)) {
    let mostrarCi = personas.Ci(parametro);

    res.json({
      mostrarCi
    })

  } else {
    let mostrarSexo = personas.Sexo(parametro);

    res.json({
      mostrarSexo
    })
  }
}


const personaPut = (req, res = response) => {

  const { id } = req.params

  if (id) {
    personas.eliminarPersona(id);
    const { nombres, apellidos, ci, direccion, sexo } = req.body;
    const persona = new Persona(nombres, apellidos, ci , direccion, sexo);

    persona.getId(id)
    personas.crearPersona(persona);
    guardarDB(personas.listArray);
    personaDB = leerDB()
  }

  res.json({
    message: 'persona editada con exito',
    personaDB
  })
}

const personaPost = (req, res = response) => {

  const { nombres, apellidos, ci, direccion, sexo } = req.body;
  const persona = new Persona(nombres, apellidos, ci, direccion, sexo);

  personas.crearPersona(persona);
  guardarDB(personas.listArray);

  const listado = leerDB();
  personas.cargarPersonasFromArray(listado)

  res.json({
    message: 'persona anadida con exito',
    listado
  })
}

const personaDelete = (req, res = response) => {

  const { id } = req.params

  if (id) {
    personas.eliminarPersona(id);
    guardarDB(personas.listArray)

    personaDB = leerDB()
  }

  res.json({
    message: 'persona eliminada con exito',
    personaDB
  })
}

module.exports = {
  personaGet,
  ciSexoPersonaGet,
  personaPut,
  personaPost,
  personaDelete
}